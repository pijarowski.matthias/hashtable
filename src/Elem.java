public class Elem {
    Object key;
    Object val;
    Elem prev = null;
    Elem next = null;

    Elem (Object key, Object val) {
        this.key = key;
        this.val = val;
    }
}

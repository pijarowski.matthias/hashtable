// Sondierungssequenz gemäß linearer Sondierung.
public class LinearProbing extends AbstractHashSequence {

    private int first_index;
    private int j = 0;

    // Lineare Sondierung mit Streuwertfunktion f.
    public LinearProbing (HashFunction f) {
        super(f);
    }

    @Override
    public int first(Object key) {
        first_index = func.compute(key);
        j = 0;
        return first_index;
    }

    @Override
    public int next() {
        j++;
        return (first_index + j) % size();
    }
}
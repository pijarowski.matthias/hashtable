import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        HashFunction f = new DivisionMethod(-5);
        HashTableChaining tableChaining = new HashTableChaining(f);
        tableChaining.put(1, "1");
        tableChaining.put(2, "abc");
        tableChaining.put(3, "2");
        tableChaining.put(4, "3");
        tableChaining.put(5, "dnf");
        tableChaining.put(1, 5);
        tableChaining.put(22, 67);

        tableChaining.dump();
    }
}

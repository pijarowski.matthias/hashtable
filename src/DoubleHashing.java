// Sondierungssequenz gemäß doppelter Streuung.
public class DoubleHashing extends AbstractHashSequence {

    int firstIndexFunc1;
    int firstIndexFunc2;
    int j = 0;

    // Zweite Streuwertfunktion.
    private HashFunction func2;

    // Doppelte Streuung mit Streuwertfunktionen f1 und f2.
    public DoubleHashing (HashFunction f1, HashFunction f2) {
        super(f1);
        func2 = f2;
    }

    @Override
    public int first(Object key) {
        firstIndexFunc1 = func.compute(key);
        firstIndexFunc2 = func2.compute(key);
        j = 0;
        return firstIndexFunc1;
    }

    @Override
    public int next() {
        j++;
        return (firstIndexFunc1 + j * firstIndexFunc2) % size();
    }
}
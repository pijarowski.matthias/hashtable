// Implementierung von Streuwerttabellen mit offener Adressierung.
public class HashTableOpenAddressing implements HashTable {

    private HashSequence sequence;
    private Elem[] tab;

    // Streuwerttabelle mit Sondierungsfunktion s.
    public HashTableOpenAddressing (HashSequence s) {
        sequence = s;
        tab = new Elem[s.size()];
    }

    @Override
    public boolean put(Object key, Object val) {
        if (key == null || val == null)
            return false;

        int index = sequence.first(key);
        for (int i = 0; i < sequence.size(); i++) {
            if (tab[index] == null) {
                tab[index] = new Elem(key, val);
                return true;
            }
            if (tab[index].key.equals(key)) {
                tab[index].val = val;
                return true;
            }
            index = sequence.next();
        }
        return false;
    }

    @Override
    public Object get(Object key) {
        if (key == null)
            return null;

        int index = sequence.first(key);
        for (int i = 0; i < sequence.size(); i++) {
            if (tab[index] != null && tab[index].key.equals(key))
                return tab[index].val;
            index = sequence.next();
        }
        return null;
    }

    @Override
    public boolean remove(Object key) {
        if (key == null)
            return false;

        int index = sequence.first(key);
        for (int i = 0; i < sequence.size(); i++) {
            if (tab[index] != null && tab[index].key.equals(key)){
                tab[index] = null;
                return true;
            }
            index = sequence.next();
        }
        return false;
    }

    @Override
    public void dump() {
        int counter = 0;
        for (Elem elem: tab) {
            if (elem == null){
                counter++;
                continue;
            }

            while (elem != null) {
                System.out.println(counter + " " + elem.key + " " + elem.val);
                elem = elem.next;
            }
            counter++;
        }
    }
}
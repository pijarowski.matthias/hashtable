// Implementierung von Streuwerttabellen mit Verkettung.
public class HashTableChaining implements HashTable {
    // Streuwerttabelle mit Streuwertfunktion f.
    private HashFunction function;
    private Elem[] tab;

    public HashTableChaining (HashFunction f) {
        function = f;
        tab = new Elem[f.size()];
    }

    @Override
    public boolean put(Object key, Object val) {
        if (key == null || val == null)
            return false;

        int tabIndex = function.compute(key);
        Elem elem = tab[tabIndex];

        // When no element is in the table it is safe to insert a first element
        if (elem == null) {
            tab[tabIndex] = new Elem(key, val);
            return true;
        }

        // if there is one element we have to check if key is already in the list
        // if it is the element is updated
        while (elem != null) {
            if (elem.key.equals(key)) {
                elem.val = val;
                return true;
            }
            elem = elem.next;
        }

        // the element was not in the list, we have to insert a new element
        Elem newElem = new Elem(key, val);
        newElem.next = tab[tabIndex];
        tab[tabIndex].prev = newElem;
        tab[tabIndex] = newElem;

        return true;
    }

    @Override
    public Object get(Object key) {
        if (key == null)
            return null;

        int tabIndex = function.compute(key);
        Elem elem = tab[tabIndex];
        if (elem == null)
            return null;

        while (elem != null) {
            if (elem.key.equals(key))
                return elem.val;
            elem = elem.next;
        }
        return null;
    }

    @Override
    public boolean remove(Object key) {
        if (key == null)
            return false;

        int tabIndex = function.compute(key);
        Elem elem = tab[tabIndex];
        if (elem == null)
            return false;

        while (elem != null) {
            if (elem.key.equals(key)){
                if (elem.prev == null) {
                    // This is the case if matching element is first Element in list
                    Elem newhead = elem.next;
                    if (newhead != null)
                        newhead.prev = null;
                    tab[tabIndex] = newhead;
                }
                else if (elem.next == null) {
                    // This is the case if matching element is last Element in list
                    elem.prev.next = null;
                }
                else {
                    // This is the case if matching element is in the middle of the list
                    elem.prev.next = elem.next;
                    elem.next.prev = elem.prev;
                }

                return true;
            }
            elem = elem.next;
        }
        return false;

    }

    @Override
    public void dump() {
        int counter = 0;
        for (Elem elem: tab) {
            if (elem == null){
                counter++;
                continue;
            }

            while (elem != null) {
                System.out.println(counter + " " + elem.key + " " + elem.val);
                elem = elem.next;
            }
            counter++;
        }
    }
}
